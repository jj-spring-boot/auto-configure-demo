package com.jj.vk.autoconfiguredemo;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class DemoApplicationTest {

    @Autowired
    ApplicationContext context;

    @Test
    public void testMain() {
        String beans = "java8ConditionApplication, onUserBeanPresenceConditionApplication, onMissingBeanConditionApplication, classConditionApplication, customConditionApplication, propertyConditionApplication, resourceConditionApplication";
        String[] split = beans.split(",");
        List<String> beanList = Arrays.stream(split).filter(s -> !s.isEmpty()).map(String::trim).collect(Collectors.toList());
        String[] beanDefinitionNames = context.getBeanDefinitionNames();
        List<String> loadedBeans = Arrays.stream(beanDefinitionNames).collect(Collectors.toList());

        Map<Boolean, List<String>> groupedBeans = beanList.stream().collect(Collectors.groupingBy(loadedBeans::contains));
        if (groupedBeans.getOrDefault(false, Collections.emptyList()).isEmpty()) {
            System.out.println("All expected beans are loaded");
        } else {
            System.out.println("Loaded beans: " + groupedBeans.get(true));
            System.out.println("Failed to load:  " + groupedBeans.get(false));
            Assert.fail("Failed to load: " + groupedBeans.get(false));
        }
    }
}