package com.jj.vk.autoconfiguredemo.class_conditions;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationClassConditionAutoconfigure {

    @Bean
    @ConditionalOnClass(Application.class)
    public Application classConditionApplication() {
        return new Application("Class condition Application");
    }
}
