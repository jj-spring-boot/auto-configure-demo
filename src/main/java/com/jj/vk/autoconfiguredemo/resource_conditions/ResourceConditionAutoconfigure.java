package com.jj.vk.autoconfiguredemo.resource_conditions;

import org.springframework.boot.autoconfigure.condition.ConditionalOnResource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ResourceConditionAutoconfigure {

    @Bean
    @ConditionalOnResource(resources = "classpath:resource_condition.conf")
    public Application resourceConditionApplication() {
        return new Application("resourceConditionApplication");
    }


    @Bean
    @ConditionalOnResource(resources = "classpath:missing_resource.conf")
    public Application resourceMissingConditionApplication() {
        return new Application("resourceMissingConditionApplication");
    }
}
