package com.jj.vk.autoconfiguredemo.bean_conditions;

public class PrerequisiteBean {
    String name;

    public PrerequisiteBean() {
    }

    public PrerequisiteBean(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "PrerequisiteBean{" +
                "name='" + name + '\'' +
                '}';
    }
}
