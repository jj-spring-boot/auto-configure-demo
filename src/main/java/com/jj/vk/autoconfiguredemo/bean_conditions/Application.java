package com.jj.vk.autoconfiguredemo.bean_conditions;

public class Application {
    private String name;

    public Application(String name) {
        this.name = name;
    }

    public Application() {
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Application{" +
                "name='" + name + '\'' +
                '}';
    }
}
