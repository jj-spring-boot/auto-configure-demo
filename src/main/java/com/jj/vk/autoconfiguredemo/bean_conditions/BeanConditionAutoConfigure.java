package com.jj.vk.autoconfiguredemo.bean_conditions;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConditionAutoConfigure {

    @Bean
    @ConditionalOnBean(name = "demoApplication")
    public Application onUserBeanPresenceConditionApplication() {
        return new Application("bean created by checking presence of userConfiguredPrerequisiteBean");
    }


    @Bean
    @ConditionalOnMissingBean(name = "beanApplication")
    public Application onMissingBeanConditionApplication() {
        return new Application("onMissingBeanConditionApplication");
    }
}
