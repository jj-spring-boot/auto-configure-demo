package com.jj.vk.autoconfiguredemo.bean_conditions;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PrereqBeanConfigure {
    @Bean("userConfiguredPrerequisiteBean")
    public PrerequisiteBean prerequisiteBean() {
        return new PrerequisiteBean("Prerequisite bean created by user");
    }
}
