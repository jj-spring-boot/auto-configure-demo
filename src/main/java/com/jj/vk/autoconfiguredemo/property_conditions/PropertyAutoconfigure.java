package com.jj.vk.autoconfiguredemo.property_conditions;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.yaml")
public class PropertyAutoconfigure {

    @Bean
    @ConditionalOnProperty(name = "createBeanOnProperty", havingValue = "create")
    @ConditionalOnMissingBean(name = "propertyConditionApplication")
    public Application propertyConditionApplication() {
        return new Application("propertyConditionApplication");
    }
}
