package com.jj.vk.autoconfiguredemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = "com.jj.vk.autoconfiguredemo")
public class DemoApplication implements CommandLineRunner {

    @Autowired
    private ApplicationContext context;

    public static void main(String[] args) {
        System.out.println("Initializing spring boot application");
        ConfigurableApplicationContext applicationContext = SpringApplication.run(DemoApplication.class, args);
        System.out.println("Initialized spring boot application");
    }

    @Override
    public void run(String... args) throws Exception {
        for (String name : context.getBeanDefinitionNames()) {
            if (name.contains("ConditionApplication")) {
//                System.out.println("bean = " + name + " : " + context.getBean(name));
                System.out.print(name + ", ");
            }
//            System.out.println("bean " + name + " : " + context.getBean(name));
        }
    }
}
