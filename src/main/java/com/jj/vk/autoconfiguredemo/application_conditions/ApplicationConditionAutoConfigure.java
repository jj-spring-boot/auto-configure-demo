package com.jj.vk.autoconfiguredemo.application_conditions;

import org.springframework.boot.autoconfigure.condition.ConditionalOnJava;
import org.springframework.boot.system.JavaVersion;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConditionAutoConfigure {
    @Bean
    @ConditionalOnJava(JavaVersion.EIGHT)
    public Application java8ConditionApplication() {
        return new Application("java8ConditionApplication");
    }
}
