package com.jj.vk.autoconfiguredemo.custom_conditions;

import org.springframework.boot.autoconfigure.condition.ConditionMessage;
import org.springframework.boot.autoconfigure.condition.ConditionOutcome;
import org.springframework.boot.autoconfigure.condition.SpringBootCondition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.util.ClassUtils;

import java.util.Collections;

@Configuration
public class CustomConditionAutoconfigure {
    @Bean
    @Conditional(CustomCondition.class)
    public Application customConditionApplication() {
        return new Application("customConditionApplication");
    }

    static class CustomCondition extends SpringBootCondition {

        @Override
        public ConditionOutcome getMatchOutcome(ConditionContext context, AnnotatedTypeMetadata metadata) {

            ConditionMessage.Builder message = ConditionMessage.forCondition("CustomCondition");
            String className = "com.jj.vk.autoconfiguredemo.custom_conditions.Application";
            return ClassUtils.isPresent(className, context.getClassLoader()) ?
                    ConditionOutcome.match(message.found("class").items(ConditionMessage.Style.NORMAL, className)) :
                    ConditionOutcome.noMatch(message.didNotFind("class", "classes").items(ConditionMessage.Style.NORMAL, Collections.singletonList(className)));
        }
    }
}
